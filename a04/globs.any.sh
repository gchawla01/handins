#!/bin/sh

[ $# -ge 1 ] || {
	cat <<- EOF
		NAME
		globs.any.sh - displays entries in the current directory that match any of the given globs

		SYNOPSIS
		$(basename $0) glob[...]

		DESCRIPTION
		Displays the names in the current directory that match ANY of the given globs.
		Each file is displayed at most once.

		EXIT STATUS
		0 if any name matched any glob
		1 if no name matched any glob
		2 if no glob given

		NOTES
		use case in to do the glob matching

		EXAMPLES

		\$ $(basename $0) '*'
		123
		abc
		abc123
		xxx

		\$ $(basename $0) '*c*' '*1*' '*z*'
		123
		abc
		abc123

	EOF
	exit 2
} >&2

file=""
total=0
pattern=""

for pattern in $*
do

	ls $pattern>//dev/null 2>&1
	if [ $? ==0]
	then
		total=$((total+1))
		#append the file list in variable "file"
		file=$file" "$pattern
	fi
done

#if total is more than 0, then exit with 0, else exit 1
if [$total -ge 1]
then 
	#display the list of files in one in a line
	echo $file|tr " " "\n"
	#exit with the ret value
	exit 0
else
	exit 1
fi